pnpm install
pnpm start

nvm alias default v18.17.1

pnpm install --save-dev @electron-forge/cli

pnpm exec electron-forge import

pnpm make

pnpm install --save-dev electron-packager

pnpm exec electron-packager . electron-quick-start --platform=win32 --arch=x64

pnpm run make -- --platform win32

pnpm install electron-builder --save-dev

pnpm install is-ci --save

./node_modules/.bin/electron-builder --prepackaged electron-quick-start-win32-x64
